/**
 * This is the api endpoint file for sr-tracker-rest-api.
 * SECURITY:
 *  currently this API has 0 security built in and anyone can call it
 *  and if this is deployed anywhere, then anyone with access to that
 *  system can use it.
 */

// package imports to establish the api
const express = require('express')
const bodyParser = require('body-parser');

var app = express()
app.use(bodyParser.json())
app.use(express.static('public'))

// sessionSr variable will store the SR values in memory
// currently this is the only API data
var sessionSr = {
    start: 0,
    end: 0,
    delta: 0
}

/**
 * GET /ping>
 * simple test to show the API is up
 */
// GET /ping
app.get('/ping', function (req, res) {
    res.end('pong')
})

/**
 * DELETE /server/shutdown
 * give the ability to remote shutdown the service
 */
app.delete('/server/shutdown', function (req, res) {
    server.close()
    res.end('Shutting down server')
})

/**
 * POST /session/sr
 * Gives the ability to completely replace sessionSr and all it's values.
 * Since sessionSr is the only value and since it is kept in memory
 * this method is also effectively a full PUT as well. 
 */
app.post('/session/sr', function (req, res) {
    sessionSr.start = req.body.start
    sessionSr.end = req.body.end
    res.status(201).json({
        message: "Successfully posted new session"
    })
})

/**
 * PUT /session/sr/start
 * replace only the start value of sessionSr
 */
app.put('/session/sr/start', function (req, res) {
    sessionSr.start = req.body.start
    res.status(201).json({
        message: "Successfully put new start value"
    })
})

/**
 * PUT /session/sr/end
 * replace onlhy the end value of sessionSr
 */
app.put('/session/sr/end', function (req, res) {
    sessionSr.end = req.body.end
    res.status(201).json({
        message: "Successfully put new end value"
    })
})

/**
 * GET /session/sr
 * GET the entire sessionSr JSON value
 */
app.get('/session/sr', function (req, res) {
    res.json(sessionSr)
})

/**
 * POST /session/delta
 * Will calculate, save, and return the difference between sessionSr.start and sessionSr.end.
 * The difference/delta will be saved in the sessionSr.delta. The entire sessionSr JSON will
 * be returned.
 */
app.post('/session/sr/delta', function (req, res) {
    sessionSr.delta = sessionSr.end - sessionSr.start
    res.json(sessionSr)
})

/**
 * Start server and listen on port 8080
 */
var server = app.listen(8080, function () {
    var host = server.address().address
    var port = server.address().port
    console.log(`example app listening on at http://${host}:${port}`)
})

module.exports = server; // exporting server for testing with mocha and chai-http