const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../app.backup.js')

const expect = chai.expect

chai.use(chaiHttp)

describe('GET /session/sr', () => {
    it('response status should be 200', (done) => {
        chai.request(app)
            .get('/session/sr')
            .end((err, res) => {
                console.log("Hello world")
                expect(res).to.have.status(200)
                done()
            })
    }),
        it('response should be json', (done) => {
            chai.request(app)
                .get('/session/sr')
                .end((err, res) => {
                    expect(res).to.be.json
                    done()
                })
        }),
        it('the response should have default data', (done) => {
            chai.request(app)
                .get('/session/sr')
                .end((err, res) => {
                    expect(res.body.start).to.equal(0)
                    done()
                })
        })
})

describe('setting new session values', () => {
    it('set new session value', (done) => {
        chai.request(app)
            .post('/session/sr')
            .send({
                start: 1000,
                end: 0
            })
            .end((err, res) => {
                // expect(res.body).to.be.json
                expect(res).to.have.status(201)
                expect(res.body.message).to.equal("Successfully posted new session")
                done()
            })
    })
    it('sessionSr should have new value', (done) => {
        chai.request(app)
            .get('/session/sr/')
            .end((err, res) => {
                expect(res.body.start).to.equal(1000)
                expect(res.body.end).to.equal(0)
                done()
            })
    })
    it('sessionSr delta should be -1000', (done) => {
        chai.request(app)
            .post('/session/sr/delta')
            .end((err, res) => {
                expect(res.body.delta).to.equal(0 - 1000)
                done()
            })
    })
})

describe('put new start and end values individually', () => {
    it('put new start value should return 201', (done) => {
        chai.request(app)
            .put('/session/sr/start')
            .send({
                start: 13
            })
            .end((err, res) => {
                expect(res).to.have.status(201)
                expect(res.body.message).to.equal("Successfully put new start value")
                done()
            })
    })
    it('put new end value should return 201', (done) => {
        chai.request(app)
            .put('/session/sr/end')
            .send({
                end: 27
            })
            .end((err, res) => {
                expect(res).to.have.status(201)
                expect(res.body.message).to.equal("Successfully put new end value")
                done()
            })
    })
})