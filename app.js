/**
 * This is the server endpoint file for stream-helmper-js
 * SECURITY:
 *  currently this API has 0 security built in and anyone can call it
 *  and if this is deployed anywhere, then anyone with access to that
 *  system can use it.
 */

// package imports to establish the api
const express = require('express')
const bodyParser = require('body-parser');
const streamHelperConfig = require('./stream-helper.config');
const { startChatBot, disconnectChatBot } = require('./src/chatbot/chatbot');
const { createChatbotDataLocation } = require('./src/chatbot/on-message-default');

var app = express()
app.use(bodyParser.json())

// allow html and css from public folder
app.use(express.static('public'))

/**
 * GET /ping>
 * simple test to show the API is up
 */
// GET /ping
app.get('/ping', function (req, res) {
    res.end('pong')
})

/**
 * DELETE /server/shutdown
 * give the ability to remote shutdown the service
 */
app.delete('/server/shutdown', function (req, res) {
    console.log(`recdeived shutdown request from ${req.ip}`)
    server.close()
    res.end('Shutting down server')
    disconnectChatBot()
})

/**
 * Start server and listen on port 8080
 */
var server = app.listen(8080, function () {
    var host = server.address().address
    var port = server.address().port
    console.log(`example app listening on at http://${host}:${port}`)
})

// setup and start anonymous chatbot
createChatbotDataLocation()
startChatBot(streamHelperConfig.chatbot)
// *********************************

module.exports = server; // exporting server for testing with mocha and chai-http