const fs = require('fs')
const configHelper = require('./configHelper')

const sampleData = {
    word: "the word",
    anotherWord: "something else",
    again: "yup yup",
    obj: {
        more: "things",
        anotherobj: {
            cmon: "dude",
            anArray: ['1', "2", "3"]
        }
    }
}
console.log(`JPJPJP:${configHelper.dataStoreLocation}`)
const sampleDataJson = JSON.stringify(sampleData, null, 4)
console.log(sampleDataJson)


function writeJsonToFile(
    data = {
        message: "dataStore.js.writeJsonToFile: given empty data file"
    },
    callback = defaultWriteCallback(err)
) {
    fs.writeFile(
        `${configHelper.dataStoreLocation}/sampleData.json`,
        sampleDataJson,
        callback
    )
}

function defaultWriteCallback(err) {
    if (err) {
        console.log("TOPANI: there was some error during write")
        console.log(`TOPANI: err.message:= ${err.message}`)
    }
}

function setupDataStore() {

}

function createMessageIdCounter() {
    if ( fs.existsSync(`${configHelper.dataStoreLocation}/`))
}