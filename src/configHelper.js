const streamHelperConfig = require("../stream-helper.config");

module.exports = {
    dataStoreLocation: currentDirDefault(streamHelperConfig.dataStoreLocation)
}

function currentDirDefault(data = "") {
    if(emptyStringDefault(data) == "")
        return "."
    else
        return data
}

function emptyStringDefault(data = "") {
    if (data == "")
        return data
    else if (data)
        return data
    else
        return ""
}
