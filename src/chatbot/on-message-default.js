const defaultMessage = {
    'channel': 'default channel',
    'message': 'default message',
    'self': false,
    'tags': {
        id: 'default id',
        username: 'default username'
    }
}

const defaultErr = {
    'errno': 0,
    'code': 'DEFAULT',
    'syscall': 'default',
    'path': 'default'
}

var defaultDataDir = './chatbotData'

const fileAlreadyExistsErr = {
    'errno': -17,
    'code': 'EEXIST',
    'syscall': 'mkdir',
    'path': './chatbotData',
    'equals': (err) => {
        return (
            fileAlreadyExistsErr.errno == err.errno &&
            fileAlreadyExistsErr.code == err.code &&
            fileAlreadyExistsErr.syscall == err.syscall
        )
    }
}

function writeMessageToDisk(message = defaultMessage) {
    const fs = require('fs')
    fs.writeFile(
        `./${message.channel.replace('#','')}.${message.tags.username}.${message.tags.id}.chatbot.msg`.toString(),
        JSON.stringify(message, null, 4),
        () => {
            console.log(`writing message to disk: ${message.tags.id}`)
        }
    )
}

function createChatbotDataLocation(dataDir = defaultDataDir) {
    const fs = require('fs')
    fs.mkdir(dataDir, (err) => {
        if (err) {
            handleDirCreateError(err)
        } else {
            console.log(`chatbot data location, ${dataDir}, has been created`)
            defaultDataDir = dataDir
        }
    })
}

function handleDirCreateError(err = defaultErr) {
    if(fileAlreadyExistsErr.equals(err)) {
        console.log(`chatbot data location, ${err.path}, already exists and is ready for data`)
    }
}

module.exports = {
    writeMessageToDisk,
    createChatbotDataLocation
}