const { writeMessageToDisk } = require('./on-message-default')

var client

function startChatBot(chatbotConfig) {
	const tmi = require('tmi.js')

	client = new tmi.Client({
		channels: chatbotConfig.channels
	})

	client.connect().then(() => {
		console.log('anon chatbot running and listening...')
	})

	client.on('message', (channel, tags, message, self) => {
		var message = {
			'channel': channel,
			'tags': tags,
			'message': message,
			'self': self
		}
		writeMessageToDisk(message)
	})
}

function disconnectChatBot() {
	if(client != undefined) {
		client.disconnect().then(() => {
			console.log('anon chatbot disconnected from chat')
		})
	}
}

module.exports = {
	startChatBot,
	disconnectChatBot
}